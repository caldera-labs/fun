# plugin .fun
This makes WordPress plugins. These plugins use Webpack, VueJS, NPM, Yarn (maybe, unclear what yarn is but you should probably install it and composer.

The main use of the plugin is to make Caldera Forms add-ons. You can use it for other types of plugins if you like and you agree with Josh's opinions on WordPress plugin development and/ or you work for Josh and he told you to use this and you don't have a reasoned objection to the requirement.

[Learn dot more](http://docs.plugin.fun?utm_source=gitlab&utm_medium=readme&utm_campaign=fun)


## Extending Caldera Forms
This generator can be used to add one or more of the following types of new capabilities to Caldera Forms:

* Add a custom field type to Caldera Forms.
* Add a payment processor to Caldera Forms.
* Add a CRM processor to Caldera Forms.
* Add a email marketing/ newsletter  processor to Caldera Forms.
* Add a custom validation processor to Caldera Forms.
* Add a generic processor to Caldera Forms.

[Learn dot more](http://docs.plugin.fun/#/caldera-forms/index?utm_source=gitlab&utm_medium=readme&utm_campaign=fun)


## Usage

### Installation
You should do this in your plugins directory as it creates a plugin.

### Build tools
If you haven't already install vue cli

```
npm install -g vue-cli

```

Also,
* npm
* composer
* yarn
* nvm

### Choose project name
- Name will be used for:
* name in package.json
* ID attribute of element Vue app is mounted in will be cf-{{name}
* name in composer.json will be "calderawp/{{name}}"
* root namespace for PSR-4 autoloader will be "calderawp\\{{name}}\\"
- Name MUST not:
* Include capital letters.
* Include numbers.
- Name MUST be:
* Only lowercase letters.
* Be one, lower-case word.

### Install
__This is the world's worst cli, but it works, be very careful or you'll have to close the window and start over.__

First time only, git clone repo into your plugins dir, by running this from your plugins dir:
```
git clone git@gitlab.com:caldera-labs/fun.git

```
Then switch to fun dir's bin bir:

```
cd fun/bin
```

Then run fun:
```
 bash fun.sh <project-name>
```

Follow prompts.

Then install new project:

```
cd ../../<project-name> && npm install && npm run init

```

#### What Did That Do?
Create a new directory containing a WordPress plugin in your current directory. This plugin has a new sub-menu page of the Caldera Forms menu. The UI is built using VueJS written in ES6, built for browser-safe use via Webpack and Babel. Also, Composer dependencies and autoloader were setup.

At this point, based on your chosen type of capabilities you are going to add, read the appropriate page in the docs:
* [Creating Caldera Forms Processors](http://docs.plugin.fun/#/caldera-forms/field-types?utm_source=gitlab&utm_medium=readme&utm_campaign=fun)
* [Creating new Caldera Forms Field Types](http://docs.plugin.fun/#/caldera-forms/processors?utm_source=gitlab&utm_medium=readme&utm_campaign=fun)


## License and attribution and significant prior art.

This is forked from [VuePack](https://github.com/egoist/vuepack).

This is heavily reliant on some composer packages from [github/netrivet](https://github.com/netrivet).

This is copyright 2017-2112 CalderaWP.

This is licensed under the terms of the GNU GPL v2+, please share with your neighbor.