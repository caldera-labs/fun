const _ = require('lodash');

const OA = 'I feel like it\'s waiting for you. To hear it in you, too.';

function toCalderaCamel(str) {

	return 'caldera' + _.camelCase( _.upperFirst(str) );
}

module.exports = {
	"prompts": {
		"name": {
			"type": "string",
			"required": true,
			"message": "HEY YOU! It is important you read the section of the README on choosing a project name."
		},
		"description": {
			"type": "string",
			"required": false,
			"message": "Project description",
			"default": "Caldera Forms add-on of industry "
		},
		"author": {
			"type": "string",
			"message": "Author",
			"default" : "Josh Pollock <josh@calderawp.com>"
		},
		"payment": {
			"type": "boolean",
			"default": false,
			"required": false,
			"message": "Add payment processor?"
		},
		"email": {
			"default": false,
			"type": "boolean",
			"required": false,
			"message": "Add email marketing processor?"
		},
		"validation": {
			"default": false,
			"type": "boolean",
			"required": false,
			"message": "Add custom validation processor?"
		},
		"crm" : {
			"default": false,
			"type": "boolean",
			"required": false,
			"message": "Add CRM processor?"
		},
		"field" : {
			"default": false,
			"type": "boolean",
			"required": false,
			"message": "Create field?"
		},
		"eslint": {
			"type": "confirm",
			"message": "Do you want to use ESLint?",
			"default": true

		},
		"testcafe": {
			"type": "confirm",
			"message": "Add testcafe to run integration tests?",
			"default": true
		}
	},
	"skipInterpolation": "client/**/*.vue",
	"filters": {
		".eslintrc": "eslint",
		"test/e2e/*": "testcafe",
		"src/Fields/**/*" : "field"
	},
	"completeMessage": "Thank you for using the Caldera WordPress plugin generator\n\n\n \""  + OA + "\"\n\n - The OA \n\n\n Please enjoy your Caldera Forming Experience\n\n  cd ../../{{destDirName}}\n  npm install && npm run init \n This will take a moment. Maybe stand up or something...",
	helpers: {
		namespace(  ){
			return 'calderawp\\' +  _.camelCase( this.name );
		},
		verConstant(){
			return _.toUpper( _.snakeCase( this.name ) ) + '_VER';
		},
		containerFunc(){
			return toCalderaCamel( this.name ) + '()'
		},
		appEl(){
			return 'cf-' + _.kebabCase( this.name );
		},
		lowercase: str => _.lowerCase(str),
		camelCase: str => _.camelCase(str),
		snakeCase: str => _.snakeCase(str),
		toCalderaPrefixedCamel: str => toCalderaCamel(str),
		ucFirst: str => _.upperFirst(str),
		toUpper: str => _.toUpper(str),
		toLower: str => _.toLower(str)
	}

};
