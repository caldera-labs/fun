<?php

namespace {{ namespace name }};

/**
 * Class Hook
 *
 * WordPress hook as entity
 *
 * @package {{ namespace name }}
 */
class Hook
{

	/**
	 * Name of hook
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $hook;

	/**
	 * Callback for hook
	 *
	 * @since 0.0.1
	 *
	 * @var string|array|callable
	 */
	protected $callback;

	/**
	 * Number of arguments for hook
	 *
	 * @since 0.0.1
	 *
	 * @var int
	 */
	protected $args = 1;

	/**
	 * Priority of hook
	 *
	 * @since 0.0.1
	 *
	 * @var int
	 */
	protected $priority = 10;



	/** @inheritdoc */
	public function __get($name)
	{
		return $this->$name;
	}

	/**
	 * Hook constructor.
	 *
	 * @since 0.0.1
	 * @param \stdClass $obj
	 */
	public function __construct( \stdClass $obj )
	{
		foreach ( get_object_vars( $this ) as $prop => $var ){
			if( isset( $obj->$prop ) ){
				$this->$prop = $obj->$prop;
			}
		}

	}

	/**
	 * Create hook from array if it is an array and valid
	 *
	 * @since 0.0.1
	 *
	 * @param $hook
	 * @return Hook
	 */
	public static  function maybeFromArray($hook): Hook
	{
		if (is_array($hook) && isset($hook['hook'], $hook['callback'])) {
			$hook = self::fromArray($hook);
		}
		return $hook;
	}


	/**
	 * Create hook from array
	 *
	 * @since 0.0.1
	 *
	 * @param $hook
	 * @return Hook
	 */
	public static function fromArray($hook): Hook
	{
		$hook = new Hook((object)$hook);
		return $hook;
	}

}