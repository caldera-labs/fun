<?php


namespace {{ namespace name }};
use {{ namespace name }}\Field\FieldFactory;
use {{ namespace name }}\Field\Field;
use {{ namespace name }}\Processors\Factory;
use {{ namespace name }}\Processors\Processor;


/**
 * Class Container
 *
 * Main container for holding global (to plugin) objects without using a singleton
 *
 * Access main instance using {{ containerFunc }};
 *
 * @package calderawp\{{name}}
 */
class Container
{

	/**
	 * Hidden Pimple Container
	 *
	 * Makes repostiroy invisible, there is intentionally no access to this externally - this class must control container access.
	 *
	 * @since 0.0.1
	 *
	 * @var \Pimple\Container
	 */
	private $container;

	/**
	 * Container constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param Config $config Config option
	 *
	 */
	public function __construct( Config $config )
	{
		$this->container = new \Pimple\Container;
		$this->container->offsetSet( 'config', $config );
	}

	/**
	 * Get config object
	 *
	 * @since 0.0.1
	 *
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->container->offsetGet( 'config' );
	}


	/**
	 * Set the main Settings object
	 *
	 * Note, will be lazy-loaded with default settings controller if this is never used
	 *
	 * @since 0.0.1
	 *
	 * @param Setting $setting
	 */
	public function setSettings( Setting $setting )
	{
		$this->container->offsetSet( 'settings', $setting );
	}

	/**
	 * Get settings object
	 *
	 * @return Setting
	 */
	public function getSettings()
	{
		//Defaults for settings can be added as method of Config, or array created here.
		$defaults = method_exists( $this->getConfig(), 'globalSettingsDefaults' )
			? $this->getConfig()->globalSettingsDefaults()
				: [
					'foo' => '42'
				];
		if( ! $this->container->offsetExists( 'settings' ) ){
			$this->setSettings(
				new Setting(
					$this->getConfig()->name,
					$defaults
				)
			);
		}
		return $this->container->offsetGet( 'settings' );
	}


	/**
	 * Get factories collection
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getFactories()
	{
		$this->maybeInitFactories();

		return $this->container->offsetGet( 'factories' );
	}

	/**
	 * Get urls object
	 *
	 * @since 0.0.1
	 *
	 * @return Urls
	 */
	public function getUrls()
	{
		$id = 'urls';
		if( ! $this->container->offsetExists( $id ) ){
			$this->container->offsetSet(
				$id,
				new Urls( $this )
			);
		}

		return $this->container->offsetGet( $id );
	}

	/**
	 * Add a factory
	 *
	 * @since 0.0.1
	 *
	 * @param FactoryService $FactoryService Factory to add
	 * @param string $key Identifier for factory
	 *
	 * @return  Container
	 */
	public function addFactory( FactoryService $FactoryService, $key )
	{
		$this->maybeInitFactories();

		$factories = $this->getFactories();
		$factories[ $key ] = $FactoryService;
		$this->setFactories( $factories );
		return $this;
	}

	/**
	 * Get factory from container
	 *
	 * @since 0.0.1
	 *
	 * @param string $key Identifier for factory
	 *
	 * @return FactoryService|null
	 */
	public function getFactory( $key )
	{
		$this->maybeInitFactories();

		if ( $this->hasFactory( $key  )) {
			return $this->container['factories'][$key];
		}

		return null;
	}

	/**
	 * Check if factory is in container
	 *
	 * @since 0.0.1
	 *
	 * @param string $key Identifier for factory
	 *
	 * @return bool
	 */
	public function hasFactory( $key )
	{
		return isset( $this->container[ 'factories' ][ $key ] );
	}

	/**
	 * If there is no key in container for factories, add it
	 *
	 * @since 0.0.1
	 */
	protected function maybeInitFactories()
	{
		if (!$this->container->offsetExists('factories')) {
			$this->setFactories([]);
		}
	}

	/**
	 * Set factories
	 *
	 * @since 0.0.1
	 *
	 * @param array $factories
	 */
	protected function setFactories(array $factories )
	{
		$this->container->offsetSet('factories', $factories );
	}


	/**
	 * Get field, registering if needed
	 *
	 * @since 0.0.1
	 *
	 * @return Field
	 */
	public function getField()
	{
		if( ! $this->container->offsetExists( 'field' ) ){
			$field = new Field();
			$this->container->offsetSet(
				'field',
				$field
			);

			$this->getFieldFactory()->newType( $field->getConfig() );

		}

		return $this->container->offsetGet( 'field' );

	}


	/**
	 * Get field factory for this plugin
	 *
	 * @since 0.0.1
	 *
	 * @return FieldFactory
	 */
	public function getFieldFactory()
	{
		if( ! $this->container->offsetExists( 'FieldFactory' ) ){
			$this->container->offsetSet(
				'FieldFactory',
				new FieldFactory( $this )
			);
		}

		return $this->container->offsetGet( 'FieldFactory' );

	}

	/**
	 * Add action
	 *
	 * All actions MUST be added through here
	 *
	 * @since 0.0.1
	 *
	 * @param Hook|array $hook Hook object or array to create one from.
	 * @return $this
	 */
	public function addAction( $hook )
	{
		$hook = Hook::maybeFromArray( $hook );
		$this->getHooks()->addAction( $hook );
		return $this;
	}

	/**
	 * Add filter
	 *
	 * All fitlers MUST be added through here
	 *
	 * @since 0.0.1
	 *
	 * @param Hook|array|string $hook Hook object or array to create one from.
	 * @param string|null|array $callback Optional. Callback function. Used when $hook is string
	 * @return $this
	 */
	public function addFilter( $hook, $callback = null )
	{
		if( is_string( $hook ) && $callback ){
			$hook = [
				'callback' => $callback,
				'hook' => $hook
			];
		}

		$hook = Hook::maybeFromArray( $hook );
		$this->getHooks()->addFilter( $hook );
		return $this;
	}


	/**
	 * (re)Set hooks system
	 *
	 * @since 0.0.1
	 *
	 * @param Hooks $hooks
	 */
	public function setHooks( Hooks $hooks )
	{
		$this->container->offsetSet( 'hooks', $hooks );
	}

	/**
	 * Get hooks system
	 *
	 * @since 0.0.1
	 *
	 * @return Hooks
	 */
	protected function getHooks()
	{
		return $this->container->offsetGet( 'hooks' );
	}

	/**
	 * Acts as Hook factory when passed array, else input MUST be Hook object
	 *
	 * @since 0.0.1
	 *
	 * @param Hook|array $hook
	 *
	 * @return Hook
	 */
	protected function maybeConvertHook($hook)
	{
		if ( is_array($hook) && isset($hook['hook'] ) ) {
			$hook = new Hook((object)$hook);
		}
		return $hook;
	}

	/**
	 * Get processor factory
	 *
	 * @return Factory|FactoryService|null
	 */
	public function getProcessorsFactory()
	{
		return $this->getFactory( 'processors' );
	}

	/**
	 * Apply a filter
	 *
	 * @since 0.0.1
	 *
	 * @param string $filter Name of filter
	 * @param mixed $value Value to apply
	 * @param array $args Optional. Args to pass to object
	 * @return $this|mixed
	 */
	public function applyFilters( $filter, $value, array $args = [] )
	{
		return $this->getHooks()->applyFilters(
			$this->maybeConvertHook( [ 'hook' => $filter ] ),
			$value,
			$args
		);
	}

	/**
	 * Do an action
	 *
	 * @since 0.0.1
	 *
	 * @param string $action Name of action to do
	 * @param array $args Optional. Args to pass to object
	 * @return $this|mixed
	 */
	public function doAction( $action, array $args = [] )
	{
		return $this->getHooks()->doAction(
			$this->maybeConvertHook( [ 'hook' => $action ] ),
			$args
		);

	}

	/**
	 * Add processor to collection
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor
	 *
	 * @return $this
	 */
	public function addProcessor( Processor $processor )
	{
		$processors[ $processor->getIdentifier() ] = $processor;
		$this->setProcessors($processors);
		return $this;
	}

	/**
	 *  Get processor from  collection
	 *
	 * @since 0.0.1
	 *
	 * @param string $identifier
	 * @return Processor
	 */
	public function getProcessor( $identifier )
	{
		if( $this->hasProcessor( $identifier ) ){
			$processors = $this->getProcessors();
			return $processors[ $identifier ];
		}

	}

	/**
	 *  Can haz processor from  collection
	 *
	 * @since 0.0.1
	 *
	 * @param string $identifier
	 * @return bool
	 */
	public function hasProcessor($identifier ){
		$processors = $this->getProcessors();
		return array_key_exists( $identifier, $processors );
	}

	/**
	 * Get all processors in collection
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	protected function getProcessors()
	{
		if( ! $this->container->offsetExists( 'processors' ) ){
			$this->setProcessors([]);
		}

		return $this->container->offsetGet( 'processors' );

	}

	/**
	 * Set processors collection
	 *
	 * @since 0.0.1
	 *
	 * @param array $processors
	 *
	 * @return $this
	 */
	protected function setProcessors( array  $processors)
	{
		$this->container->offsetSet('processors', $processors );
		return $this;
	}


}
