<?php

namespace {{ namespace name }}\Api;

/**
 * Class AdminApi
 *
 * Creates admin API for add-on settings
 *
 * @package {{ namespace name }}
 */
class AdminApi extends Api
{



	/**
	 * Get all routes for this API
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getRoutes()
	{
		$read = new Endpoint(
			function( \WP_REST_Request $request ){
				return rest_ensure_response( $this->getContainer()->getSettings()->get_settings() );
			}
		);

		$write = new Endpoint(
			function( \WP_REST_Request $request ){
				$settings = $this->getContainer()->getSettings()->save_settings(
					[
						'foo' => $request[ 'foo' ]
					]
				);

				return rest_ensure_response( $settings );


			},
			[
				'foo' => [
					'type' => [
						'required' => true,
						'type' => 'string'
					]
				]
			],
			[ 'POST' ]
		);

		$route = new Route(
			$this->getBase(),
			[
				'read' => $read,
				'write' => $write
			]
		);

		return [
			$route
		];
	}


}