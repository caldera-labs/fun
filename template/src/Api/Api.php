<?php

namespace {{ namespace name }}\Api;

use {{ namespace name }}\Service;


/**
 * Class Api
 *
 * Creates API routes with endpoints
 *
 * @package {{ namespace name }}
 */
abstract  class Api extends Service
{

	/**
	 * Route collection
	 *
	 * MUST return an array with one or more Route objects
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	abstract public function getRoutes();

	/**
	 * Add hooks with WordPress plugins API
	 *
	 * @since 0.0.1
	 *
	 */
	public function addHooks()
	{
		add_action( 'caldera_forms_rest_api_pre_init', [ $this, 'registerRoutes' ] );
	}

	/**
	 * Register routes
	 *
	 * @since 0.0.1
	 *
	 * @param \Caldera_Forms_API_Load $cfApi
	 */
	public function registerRoutes( \Caldera_Forms_API_Load $cfApi )
	{
		foreach ( $this->getRoutes() as $route ){
			$cfApi->add_route( $route );
		}

	}

	/**
	 * Get route base
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	protected function getBase()
	{
		return $this->getContainer()->getUrls()->apiBase();
	}

}