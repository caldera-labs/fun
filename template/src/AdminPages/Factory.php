<?php

namespace {{ namespace name }}\AdminPages;


use {{ namespace name }}\FactoryService;

/**
 * Class Factory
 *
 * Admin page factory
 *
 * @package calderawp\{{name}}\AdminPages
 */
class Factory extends FactoryService
{

	/**
	 * Create a page
	 *
	 * @since 0.0.10
	 *
	 */
	public function page(){


		$scripts = new Scripts(
			$this->getContainer()->getConfig()->distUrl,
			$this->getContainer()->getConfig()->distDir,
			$this->getContainer()->getConfig()->slug,
			$this->data()
		);

		$page =  new Page(
			$this->getContainer()->getConfig()->slug,
			$scripts
		);

		add_action( 'admin_menu', [ $page, 'display' ] );
	}

	/**
	 * Data to print to dom
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function data()
	{

		$data = [
			'api' => [
				'url' => esc_url_raw( $this->getContainer()->getUrls()->apiUrl() ),
				'nonce' => \Caldera_Forms_API_Util::get_core_nonce()
			],
			'strings' => [

			]
		];

		return $data;
	}

}