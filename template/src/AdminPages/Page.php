<?php

namespace {{ namespace name }}\AdminPages;

/**
 * Class Page
 *
 * Creates a the admin page
 *
 * @package {{ namespace name }}
 */
class Page
{

	/**
	 * Plugin slug
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * Admin app system
	 *
	 * @since 0.0.1
	 *
	 * @var Scripts
	 */
	protected $scripts;

	/**
	 * Page constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param string $slug
	 * @param Scripts $scripts
	 */
	public function __construct( $slug, Scripts $scripts )
	{
		$this->slug = $slug;
		$this->scripts = $scripts;
	}


	/**
	 * Create admin page view
	 *
	 * @since 0.1.0
	 */
	public function display() {
		add_submenu_page(
			\Caldera_Forms::PLUGIN_SLUG,
			__( {{ ucFirst name }}, '{{ lowercase name}}' ),
			__( {{ ucFirst name }}, '{{ lowercase name}}' ),
			'manage_options',
			$this->slug,
			[ $this, 'render' ]
		);
	}

	/**
	 * Redner admin page view
	 *
	 * @since 0.1.0
	 */
	public function render() {
		echo $this->scripts->webpack( );
	}


}