<?php


namespace {{ namespace name }};

/**
 * Trait MagicallyHasContainer
 *
 * Automagic injection of Container <em>like, anywhere</em>. This smells so much like global state, it makes me nervous...
 *
 * @package {{ namespace name }}
 */
trait MagicallyHasContainer
{

	/**
	 * Get container
	 *
	 * @since 0.0.1
	 *
	 * @return Container
	 */
	protected function getContainer()
	{
		return {{ containerFunc }};
	}

}