<?php


namespace {{ namespace name }};

/**
 * Factory
 *
 * Object factory service
 *
 * @package {{ namespace name }};
 */
abstract  class  FactoryService extends Service
{

}