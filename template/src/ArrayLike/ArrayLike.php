<?php
namespace {{ namespace name }};

/**
 * Class ArrayLike
 *
 * Most basic ArrayAccess implementation. Anything that is currently an array, but SHOULD have an object representation, MUST use this instead of a normal array.
 *
 * @package calderawp\\{name}}
 */
class ArrayLike implements \ArrayAccess
{
	/**
	 * Collection of items
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 * Get all items as array
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->items;
	}

	/** @inheritdoc */
	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->items[] = $value;
		} else {
			$this->items[$offset] = $value;
		}
	}

	/** @inheritdoc */
	public function offsetExists($offset)
	{
		return isset($this->items[$offset]);
	}

	/** @inheritdoc */
	public function offsetUnset($offset)
	{
		unset($this->items[$offset]);
	}

	/** @inheritdoc */
	public function offsetGet($offset)
	{
		return isset($this->items[$offset]) ? $this->items[$offset] : null;
	}

}