<?php


namespace {{ namespace name }};


/**
 * Class Setting
 *
 * Storing setting as option abstraction
 *
 * @package {{ namespace name }}\Settings
 */
class Setting extends \Caldera_Forms_Settings_Option
{

	/**
	 * Name of setting
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Defaults for array saved in option
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Setting constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param $name
	 * @param array $defaults
	 */
	public function __construct( $name, array $defaults = [] )
	{
		$this->name = $name;
		$this->defaults = is_array( $this->defaults )
			? array_merge(
				$this->defaults,
				$defaults
			)
			: $defaults;

	}

	/** @inheritdoc */
	public function get_name()
	{
		return $this->name;
	}


	/** @inheritdoc */
	public function get_option_key(){
		return '_cf_' . strtolower( $this->name );
	}

	/** @inheritdoc */
	protected function prepare( array $settings ){
		foreach ( $settings as $setting => $value ){
			if( ! array_key_exists( $setting, $this->defaults ) ){
				unset( $settings[ $setting ] );
			}

		}

		return $settings;
	}

	/** @inheritdoc */
	protected function get_defaults(){
		return $this->defaults;
	}

}