<?php
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

$adminFields = {{ containerFunc }}->getField()->getAdminFields();
if( ! empty( $adminFields ) ) {
	$string = Caldera_Forms_Processor_UI::config_fields($adminFields);
	echo $string;
}

