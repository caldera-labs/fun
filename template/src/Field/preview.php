<?php
if( ! defined( 'ABSPATH' ) ){
	exit;
}

?>
<div class="preview-caldera-config-group">
    {{config/before}} {{#if config/default}} {{config/default}}{{else}}400{{/if}} {{config/after}}
</div>