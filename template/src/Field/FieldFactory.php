<?php


namespace {{ namespace name }}\Field;


use {{ namespace name }}\FactoryService;
use {{ namespace name }}\Hook;

/**
 * Class FieldFactory
 *
 * Single function field registration from config array
 *
 * @package {{ namespace name }}
 */
class FieldFactory extends FactoryService
{

	/**
	 * Register a new field type
	 *
	 * @since 0.0.1
	 *
	 * @param array $config
	 */
	public function newType( array  $config )
	{
		$field = new Register( $this->getContainer() );
		$field->setField( $this->getContainer()->getField() );


		$this->getContainer()->addFilter(
			[
				'hook' => 'caldera_forms_get_field_types',
				'callback' => [ $field, 'registerField' ]
			]
		);

		$filter = 'caldera_forms_field_attributes-' . $field->getIdentifier();
		$this->getContainer()->addFilter(
			[
				'hook' => $filter,
				'callback' => [ $field, 'filterAttrs' ]
			]
		);

	
	}

}