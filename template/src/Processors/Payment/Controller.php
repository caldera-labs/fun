<?php


namespace {{ namespace name }}\Processors\Payment;


use {{ namespace name }}\Processors\ControllerInterfaces\Payment;
use {{ namespace name }}\Processors\Data;

/**
 * Class Controller
 *
 * This is where the actual processing of the payment processor happens.
 *
 * @package calderawp\{[name}}
 */
class Controller implements Payment
{
	/** @inheritdoc */
	public function pre( Data $data )
	{

	}

	/** @inheritdoc */
	public function process( Data $data )
	{

	}

	/** @inheritdoc */
	public function post( Data $data )
	{

	}

	/** @inheritdoc */
	public function pay(Data $data)
	{

	}

}