<?php


namespace {{ namespace name }}\Processors\Crm;


use {{ namespace name }}\Processors\ControllerInterfaces\Crm;
use {{ namespace name }}\Processors\ControllerInterfaces\Payment;
use {{ namespace name }}\Processors\Data;
/**
 * Class Controller
 *
 * This is where the actual processing of the crm processor happens.
 *
 * @package calderawp\{[name}}
 */
class Controller implements Crm
{
	/** @inheritdoc */
	public function pre( Data $data )
	{

	}

	/** @inheritdoc */
	public function process( Data $data )
	{

	}

	/** @inheritdoc */
	public function post( Data $data )
	{

	}

	/** @inheritdoc */
	public function pay( Data $data)
	{

	}

	/** @inheritdoc */
	public function sendViaEmail( Data $data, $args )
	{

	}

}