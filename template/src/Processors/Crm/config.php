<?php
if( ! defined( 'ABSPATH' ) ){
	exit;
}

$processor = {{ containerFunc }}->getProcessor( '{{name}}Crm' );
if( $processor ){
	$fields = $processor->getAdminFields();
	if( $fields ){
		echo \Caldera_Forms_Processor_UI::config_fields( $fields );
	}

}