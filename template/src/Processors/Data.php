<?php

namespace {{ namespace name }}\Processors;


class Data
{

	/**
	 * Submission data
	 *
	 * @var \Caldera_Forms_Processor_Get_Data
	 */
	protected $data;

	/**
	 * Processor entity that is being processed currently
	 *
	 * @since 0.0.1
	 *
	 * @var Process
	 */
	protected $processor;

	/**
	 * Form config
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $form;

	/**
	 * Callbacks constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param \Caldera_Forms_Processor_Get_Data $data
	 * @param Process $processor
	 * @param array $form
	 */
	public function __construct(\Caldera_Forms_Processor_Get_Data $data, Process $processor, array $form )
	{
		$this->data = $data;
		$this->processor = $processor;
		$this->form = $form;

	}

	/**
	 * Get the fields values of processor
	 *
	 * @since 0.0.1
	 *
	 * @return array|null
	 */
	public function getFields()
	{
		return  $this->data->get_fields();
	}

	/**
	 * Get field values of processor
	 *
	 * @since 0.0.1
	 *
	 * @return array|null
	 */
	public function getValues(){
		return $this->data->get_values();
	}

	/**
	 * Get form config
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getForm()
	{
		return $this->form;
	}

	/**
	 * Report an error
	 *
	 * @since 0.0.1
	 *
	 * @param string $error
	 */
	public function addError(  $error )
	{
		$this->data->add_error( $error );
	}

	/**
	 * Get errors, if they exist
	 *
	 * @since 0.0.1
	 *
	 * @return array|null
	 */
	public function getErrors()
	{
		return $this->data->get_errors();
	}


}