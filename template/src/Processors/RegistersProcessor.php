<?php


namespace {{ namespace name }}\Processors;


use {{ namespace name }}\MagicallyHasContainer;

/**
 * Trait RegistersProcessor
 *
 * This trait does all of the work to register processing based on a Processor entity
 *
 * @package {{ namespace name }}\Processors
 */
trait RegistersProcessor
{


	use MagicallyHasContainer;

	/**
	 * Register processor with container
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor
	 */
	public function registerProcessor( Processor $processor )
	{
		$this->getContainer()
			->addProcessor($processor )
			->getProcessorsFactory()
			->addProcessor( $processor );
	}
}
