<?php


namespace {{ namespace name }}\Processors;
use {{ namespace name }}\MagicallyHasContainer;

/**
 * Class Register
 *
 * Registers a plugin with the WordPress plugins API and therefore Caldera Forms
 *
 * @package {{ namespace name }}\Processors
 */
class Register
{

	use MagicallyHasContainer;

	/**
	 * Processor entity
	 *
	 * @since 0.0.1
	 *
	 * @var Processor
	 */
	protected $processor;

	/**
	 * Register constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor
	 */
	public function __construct( Processor $processor )
	{
		$this->processor = $processor;
	}

	/**
	 * Register processor
	 *
	 * @since 0.0.1
	 *
	 * @uses "caldera_forms_get_form_processors" filter
	 *
	 * @param array $processors
	 *
	 * @return array
	 */
	public function registerProcessor( $processors ){
		$dispatcher = $this->processor->toDispatcher();
		$processors[$this->processor->getIdentifier()] = array_merge(
			$this->processor->getMetaDetails(),
			[
				'pre_processor' => [ $dispatcher, 'pre_process' ],
				'processor' => [ $dispatcher, 'process' ],
				'post_processor' => [ $dispatcher, 'process' ],
				'template' => $this->configFilePath()
			]
		);

		return $processors;

	}


	/**
	 * Add new form templates to make it easier to get started with this processor
	 *
	 * @since 0.0.1
	 *
	 * @uses "caldera_forms_get_form_templates" filter
	 *
	 * @param array $templates
	 *
	 * @return array
	 */
	public function formsTemplates( $templates )
	{
		return $templates;
	}

	/**
	 * Get path to config file for processor
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	protected function configFilePath(): string
	{
		return plugin_dir_path(__FILE__) . ucwords( $this->processor->getType() ) . '/config.php';
	}
}