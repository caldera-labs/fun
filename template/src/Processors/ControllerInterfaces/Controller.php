<?php


namespace {{ namespace name }}\Processors\ControllerInterfaces;


use {{ namespace name }}\Processors\Data;

/**
 * Interface Controller
 *
 * Interface all processor controllers MUST implement
 *
 * @package {{ namespace name }}
 */
interface Controller
{
	/**
	 * Run pre process step
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 */
	public function pre( Data $data );

	/**
	 * Run  process step
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 */
	public function process( Data $data );

	/**
	 * Run post process step
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 */
	public function post( Data $data );
}