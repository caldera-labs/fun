<?php


namespace {{ namespace name }}\Processors\ControllerInterfaces;


use {{ namespace name }}\Processors\Data;

/**
 * Interface Crm
 *
 * CRM processor controllers MUST implement this
 *
 * @package calderawp\\{name}}
 */
interface Crm extends Controller
{
	/**
	 * Update via email
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 * @param $args
	 */
	public function sendViaEmail( Data $data, $args );

}