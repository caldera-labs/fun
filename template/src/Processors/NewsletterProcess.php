<?php
namespace {{ namespace name }}\Processors;


class NewsletterProcess extends Process implements  \Caldera_Forms_Processor_Interface_Newsletter
{

	/** @inheritdoc */
	public function subscribe( array $subscriber_data, $list_name )
	{
		$this->dispatch(
			'subscribe',
			[
				'subscriber_data' => $subscriber_data,
				'list_name' => $list_name
			]

		);
	}

	/** @inheritdoc   */
	protected $dispatchableAts = [
		'subscribe'
	];

}