<?php


namespace {{ namespace name }}\Processors;

/**
 * Class Crm
 *
 * Process dispatcher for Crm processors
 *
 * @package {{ namespace name }}
 */
class CrmProcess extends Process implements   \Caldera_Forms_Processor_Interface_CRM
{

	/** @inheritdoc   */
	protected $dispatchableAts = [
		'update_by_email'
	];

	/** @inheritdoc */
	public function update_by_email( $email, array $form_data )
	{

		$this->dispatch(
			'update_by_email',
			[
				'email' => $email,
				'form_data' => $form_data
			]

		);

	}

}