<?php


namespace {{ namespace name }}\Processors\Newsletter;

use {{ namespace name }}\Controlled;
use {{ namespace name }}\Processors\Processor as ParentClass;
use {{ namespace name }}\Processors\RegistersProcessor;

/**
 * Class Processor
 *
 * Defines a newsletter/ email marketting processor entity

 * @package {{ namespace name }}
 */
class Processor extends ParentClass
{

	use RegistersProcessor;
	use Controlled;

	/** @inheirtdoc   */
	protected $type = 'newsletter';

	/** @inheirtdoc   */
	protected $identifier = '{{name}}Newsletter';

	/**
	 * Processor constructor.
	 *
	 * @since 0.0.1
	 */
	public function __construct()
	{
		$this->registerProcessor( $this );
		$this->control( $this );
	}

	/**
	 * Get meta info for the plugin like author and suchs
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getMetaDetails()
	{
		return [
			'name' 			=> $this->processorName(),
			'description' 	=> '{{ description }}',
			'author' 		=> '{{ author }}',
			'author_url' 	=> 'https://calderaforms.com'
		];
	}

	/**
	 * Get admin fields
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getAdminFields()
	{

		//SEE \Caldera_Forms_Processor_UI::config_field() for field tyeps
		return [
			[
				'type' => 'number',
				'required' => 'false',
				'magic' => true,
				'id' => 'size',
				'label' => __('Size', '{{name}}')
			],
			[
				'type' => 'dropdown',
				'options' => [
					'no' => __('No'),
					'yes' => __('Yes')
				],
				'id' => 'allow',
				'label' => __('allow', '{{name}}')
			]

		];
	}



}