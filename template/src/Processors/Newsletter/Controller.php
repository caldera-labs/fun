<?php

namespace {{ namespace name }}\Processors\Newsletter;

use {{ namespace name }}\Processors\ControllerInterfaces\Newsletter;
use {{ namespace name }}\Processors\Data;

/**
 * Class Controller
 *
 * This is where the actual processing of the email marketing/newsletter processor happens.
 *
 * @package calderawp\{[name}}
 */
class Controller implements Newsletter
{
	/** @inheritdoc */
	public function pre( Data $data )
	{

	}

	/** @inheritdoc */
	public function process( Data $data )
	{

	}

	/** @inheritdoc */
	public function post( Data $data )
	{

	}

	/** @inheritdoc */
	public function pay( Data $data)
	{

	}

	/** @inheritdoc */
	public function subscribe( Data $data, array  $args )
	{

	}

}