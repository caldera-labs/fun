<?php
/**
 * Plugin Name: {{ ucFirst name}}
 * Description: {{description}}
 * Version: 0.0.1
 */

use {{ namespace name }}\Container as Container;
use {{ namespace name }}\Config as Config;
use {{ namespace name }}\Hooks as Hooks;


/**
 * Plugin version
 *
 * @since 0.0.1
 */
define( '{{verConstant}}', '0.0.1' );


add_action( 'init', function(){
	include_once  __DIR__ . '/vendor/autoload.php';
	if( defined( 'CFCORE_VER' ) ){
		{{ toCalderaPrefixedCamel name }}();
	}else{

	}

});

/**
 * Get main instance of container
 *
 * @return Container
 */
function {{ containerFunc }}{
	static  ${{ toCalderaPrefixedCamel name }};

	if( ! is_object( ${{ toCalderaPrefixedCamel name }} ) ){
		include_once  __DIR__ . '/vendor/autoload.php';


		$config = Config::factory( (object) [
			'name' => '{{name}}',
			'version' => {{verConstant}},
			'distDir' =>  __DIR__ . '/dist',
			'distUrl' => plugin_dir_url( __FILE__  ) . 'dist/',
			'slug' => '{{snakeCase name}}'
		]);

		${{ toCalderaPrefixedCamel name }} = new Container( $config );

		/**
		 * Runs when main instance of container is initialized
		 *
		 * @param Container ${{ toCalderaPrefixedCamel name }} instance of container when initialized
		 */
		do_action( 'caldera.{{name}}.init', ${{ toCalderaPrefixedCamel name }} );
	}

	return ${{ toCalderaPrefixedCamel name }};

}


/**
 * Register some services
 */
add_action( 'caldera.{{name}}.init',
	function( Container $container ){

		//Add admin page factory
		$container->addFactory(
			new {{ namespace name }}\AdminPages\Factory( $container ),
			'admin'
		);
		$container->getFactory( 'admin' )->page();

		//Create REST API for settings, also settings storage.
		$adminApi = new {{ namespace name }}\Api\AdminApi( $container );
		$adminApi->addHooks();

		//Setup plugins API abstraction
		$container->setHooks(
			new Hooks(
				new \NetRivet\WordPress\EventEmitter()
			)
		);

		//Create REST API for settings, also settings storage.
		$adminApi = new {{ namespace name }}\Api\AdminApi( $container );
		$adminApi->addHooks();

		{{#field}}
		//Register new field type
		$container->getField();
		{{/field}}

		//Add processor factory
		$container->addFactory(
			new {{ namespace name }}\Processors\Factory( $container ),
			'processors'
		);

		{{#payment}}
		//Add a payment processor
		$payment = new {{ namespace name }}\Processors\Payment\Processor();
		{{/payment}}

		{{#crm}}
		//Add a CRM processor
		$payment = new {{ namespace name }}\Processors\Crm\Processor();
		{{/crm}}

		{{#newsletter}}
		//Add a newsletter/ email marketing  processor
		$payment = new {{ namespace name }}\Processors\Newsletter\Processor();
		{{/newsletter}}




	},
	2
);

