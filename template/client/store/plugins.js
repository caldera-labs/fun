/** Import plugin factory **/
import { createStatePlugin } from "vue-app-state";

/**
 * Define your 2-way, 1-way and 2-way async bindings
 */
const stateComputes = {
	//2-way bindings, Must have getter and mutation with same name to get/set this prop
	twoWay: [
		'foo',
		'mainStatus'
	],
	//1-way/ read-only bindings. Must have getter with same name to use to get value.
	oneWay: [
		'hi'
	],
	//2-way async bindings. Must have a getter with same name to get value and an action of same name to dispatch when updated
	dispatching: [
	]
};

export  const  statePlugin = new createStatePlugin( stateComputes );