
import axios from 'axios';

export  const GETTERS = {
	foo: state => {
		return state.hasOwnProperty('foo' ) ? state.foo : 'oof';
	},
	mainStatus: state => {
		return state.mainStatus;
	},
	api: state => {
		if( state.hasOwnProperty( 'api' ) ){
			return state.api;
		}

		state.api   = axios.create({
			baseURL: state.apiInfo.url,
			timeout: 30000,
			headers: {'X-WP-Nonce': state.apiInfo.nonce}
		});

		return state.api;
	},
	notSaving: state => {
		state.saving = false;
	},
	saving: state => {
		state.saving = true;
	}
};