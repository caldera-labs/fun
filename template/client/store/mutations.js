export const MUTATIONS = {
	foo(state, value ){
		state.foo = value;
	},
	mainStatus(state,value){
		state.mainStatus = {
			success: value.hasOwnProperty( 'success' ) ? value.success : false,
			message: value.hasOwnProperty( 'message' ) ? value.message : 'info',
			show: value.hasOwnProperty( 'show' ) ? value.show : false,
		};

	}
};


