import Promise from '../promise-polyfill';
const timeout = 30000;



export const ACTIONS = {
	foo(context, payload ){

	},
	save (context) {
		return context.getters.api.post(
			'/',
			{
				foo: context.getters.foo,
				bar: 'taco'
			}
		)
			.then(r =>  {
				return true;
			})
			.catch( error => {
				console.log(error);
				return true;
			});
	},
	getFoo(context){
		return context.getters.api.get(
			'/'
		)
			.then(r =>  {
				this.commit( 'foo', r.data.foo );
				return true;
			})
			.catch( error => {
				console.log(error);
				return true;
			});
	},
	notSaving ({ commit }) {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				commit( 'notSaving' );
				resolve()
			}, 500  )
		})
	},
	saving ({ commit }) {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				commit( 'saving' );
				resolve()
			}, 500  )
		})
	},



};