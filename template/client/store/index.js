import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { MUTATIONS } from './mutations';
import { ACTIONS } from './actions';
import  { GETTERS } from './getters';

const STATE = {
	foo: 'bar',
	hi: 'Roy',
	mainStatus: {
		message: 'Main status indicator message',
		success: true,
		show: true
	},
	apiInfo: {{toUpper name}}_ADMIN.api,
	strings: {{toUpper name}}_ADMIN.strings
};


import  { statePlugin } from './plugins';

const PLUGINS = [
	statePlugin
];

Vue.use(statePlugin);

const store = new Vuex.Store({
	strict: true,
	plugins: [],
	modules: {},
	state: STATE,
	getters: GETTERS,
	mutations: MUTATIONS,
	actions: ACTIONS
});

export default store;
