import './promise-polyfill';
import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import router from './router';
import store from './store';

sync(store, router);
const app = new Vue({
    el: '#{{appEl}}',
    render(h){
      return h( 'router-view' );
    },
	router,
	store,
});


