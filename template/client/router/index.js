import Vue from 'vue'
import Router from 'vue-router'
import SettingsPage from '../views/SettingsPage'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: SettingsPage
    }
  ]
})
